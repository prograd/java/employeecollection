public class Main {
    public static void main(String[] args) {
        Employee employee1=new Employee(1,10000,"IT");
        Employee employee2=new Employee(2,20000,"IT");
        Employee employee3=new Employee(3,30000,"IT");

         Company company=new Company();
         company.addEmployee(employee1);
        company.addEmployee(employee2);
        company.addEmployee(employee3);
        company.displayAllEmployees();
        company.employeeResign(1);
        System.out.println("After resign");
        company.displayAllEmployees();

    }

}
