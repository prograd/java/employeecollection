public class Employee {
    private Integer employeeId;
    private Integer salary;
    private String departmentName;

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", salary=" + salary +
                ", departmentName='" + departmentName + '\'' +
                '}';
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Employee(Integer employeeId, Integer salary, String departmentName) {
        this.employeeId = employeeId;
        this.salary = salary;
        this.departmentName = departmentName;
    }
}
