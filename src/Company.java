import java.util.ArrayList;

public class Company {
    public Company( ) {
        this.employeeList=new ArrayList<>();
    }


    private ArrayList<Employee> employeeList;

    public void addEmployee(Employee employee){
        employeeList.add(employee);
    }
    public void employeeResign(Integer employeeId){
        Employee toRemove=null;
        for (Employee emp:employeeList) {
            if(employeeId==emp.getEmployeeId()){
                toRemove=emp;
            }
        }
        employeeList.remove(toRemove);
    }

    public void displayAllEmployees(){
        for (Employee emp : employeeList) {
            System.out.println(emp);
        }
    }
}
